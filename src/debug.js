//>>excludeStart('require', pragmas.require);
define([
    "./core"
], function (Fw) {
    "use strict";
    //>>excludeEnd('require');
    (function (Fw) {
        var debug = {};
        Fw.log = function () {
            var args = [].slice.call(arguments),
                i,
                argsLength = args.length,
                log = document.createElement('div'),
                text = '';
            log.style.position = 'absolute';
            log.style.bottom = 0;
            log.style.right = 0;
            log.style.zIndex = 10000;
            log.style.color = 'red';
            for (i = 0; i< argsLength; i++) {
                if (text) {
                    text += ', ';
                }
                text += args[i];
            }
            log.innerHTML += '<div>' + text + '</div>';
        };
    }(window.Fw));
    //>>excludeStart('require', pragmas.require);
    return window.Fw;
});
//>>excludeEnd('require');
