//>>excludeStart('require', pragmas.require);
define([
    "./core"
], function (Fw) {
    "use strict";
    //>>excludeEnd('require');
    (function (Fw) {
        Fw.layout = Fw.layout || {};
    }(window.Fw));
    //>>excludeStart('require', pragmas.require);
    return Fw.layout;
});
//>>excludeEnd('require');
