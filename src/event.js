//>>excludeStart('require', pragmas.require);
define([
    "./core"
], function (Fw) {
    "use strict";
    //>>excludeEnd('require');
    (function () {
        if (typeof window.CustomEvent !== "function") {
            var CustomEvent = function (event, params) {
                params = params || { bubbles:false, cancelable:false, detail:undefined };
                var evt = document.createEvent('CustomEvent');
                evt.initCustomEvent(event, params.bubbles, params.cancelable, params.detail);
                return evt;
            };

            window.CustomEvent = CustomEvent;
        }
    })();

    (function (Fw) {
        Fw.event = Fw.event || {};

        Fw.event.trigger = function (element, type, detail) {
            var event = new CustomEvent(type, {
                detail:detail
            });
            element.dispatchEvent(event);
        };
    }(window.Fw));
    //>>excludeStart('require', pragmas.require);
    return Fw.event;
});
//>>excludeEnd('require');
