//>>excludeStart('require', pragmas.require);
define([
    "../user",
    "../base",
    "../../view/user/login",
    "../../remote"
], function (user, Base, LoginView, remote) {
    "use strict";
    //>>excludeEnd('require');
    (function (document, user, Base, LoginView, remote) {
        var Login = function () {
            var view,
                submit,
                controller = this;

            this.view = new LoginView();
            view = this.view;
            view.getHomeButton().onclick = function () {
                Base.get('signup.start').run();
            };
            submit = view.getFormButton();
            submit.addEventListener('click', function() {
                var form = view.getForm(),
                    data = new FormData(form),
                    url = 'https://gymnavigator.com/app_dev.php/api/user/login';
                Base.get('waiting').run();
                remote.getJSON(url, data, function (data) {
                if (data.status === 'ERR') {
                    view.setInfo(data.message);
                    controller.run();
                }
                else {
                    localStorage['currentUser'] = JSON.stringify(data);
                    localStorage['currentUserId'] = data.id;
                    controller.onLogin();
                }
                }, 'POST');
            });
        };

        Login.prototype = new Base();

        Login.prototype.setOnLogin = function (callback) {
            this.onLogin = callback;
        };

        Login.prototype.show = function () {
            this.view.setActive(true);
        };

        Login.prototype.hide = function () {
            this.view.setActive(false);
        };

        user.Login = Login;
    }(document, window.Fw.controller.user, window.Fw.controller.Base, window.Fw.view.user.Login, window.Fw.remote));
    //>>excludeStart('require', pragmas.require);
    return user.Login;
});
//>>excludeEnd('require');
