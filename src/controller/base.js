//>>excludeStart('require', pragmas.require);
define([
    "../controller",
    '../api/google/analytics/request'
], function (controller, analytics) {
    "use strict";
    //>>excludeEnd('require');

    (function (document, controller, analytics) {
        var Base = function () {
            },
            ads = 0,
            routers = [],
            activeController = null,
            history = [];

        Base.prototype.register = function (route) {
            this.route = route;
            routers[route] = this;
        };

        Base.get = function (route) {
            return routers[route];
        };

        Base.prototype.destroy = function (route) {
            routers[this.route] = null;
            if (this.view) {
                this.view.destry();
            }
        };

        Base.prototype.run = function (back) {
            var self = this;
            if (activeController) {
                activeController.hide();
                if (!back) {
                    history.push(activeController.route);
                }
            }
            activeController = self;
            self.show(back);
            window.location.hash = '#' + self.route;
            if (self.view && !ads) {
                self.view.reloadAds();
                ads = 1;
            }
            if (analytics) {
                analytics.page({url: self.route});
            }
        };

        Base.prototype.back = function () {
            var name = history.pop();
            while (name && name != this.route) {
                name = history.pop();
            }
            this.run(true);
        };

        controller.Base = Base;

        //>>excludeStart('require', pragmas.cordova);
        Base.popstate = function(event) {
            var name = history.pop();
            if (name) {
                Base.get(name).run(true);
            }
        }

        document.addEventListener('deviceready', function() {
            document.addEventListener('backbutton', Base.popstate, false);
        });
        //>>excludeEnd('require');

    }(document,
        window.Fw.controller,
        window.Fw.api.google.analytics.request
    ));
    //>>excludeStart('require', pragmas.require);
    return controller.Base;
});
//>>excludeEnd('require');
