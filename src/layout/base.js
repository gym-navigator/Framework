//>>excludeStart('require', pragmas.require);
define([
    "../layout",
    'less!fw/less/body'
], function (layout) {
    "use strict";
    //>>excludeEnd('require');

    (function (document, layout) {
        var Base = function () {
        };

        Base.prototype.setParent = function (parent) {
            if (parent) {
                parent.appendChild(this._ui.container);
            }
        };

        Base.prototype.getContainer = function () {
            return this._ui.container;
        };

        Base.prototype.destroy = function () {
            var ui = this._ui;
            if (ui.container && ui.container.parentNode) {
                ui.container.parentNode.removeChild(ui.container);
            }
        };

        layout.Base = Base;
    }(document, window.Fw.layout));
    //>>excludeStart('require', pragmas.require);
    return layout.Base;
});
//>>excludeEnd('require');
