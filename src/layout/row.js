(function (document, layout) {
    "use strict";
//>>excludeStart('require', pragmas.require);
    define([
        "../layout"
    ], function () {
        //>>excludeEnd('require');

        var Row = function (options) {
            this.options = {
                parent:options.parent,
                heigth: options.heigth || '60px'
            };
            this._ui = {
                elements: []
            };
            this.create(this.options.parent);
            this.count = 0;
            this.elements = [];
        };

        Row.prototype.create = function (parent) {
            var ui = this._ui;
            ui.container = document.createElement('div');
            ui.container.classList.add('ui-row');
            ui.container.style.heigth = this.options.height;
            parent.appendChild(ui.container);
        };

        Row.prototype.add = function (element, colspan) {
            var newElement = document.createElement('div'),
                newElementStyle = newElement.style;
            colspan = colspan || 1;
            this.count += colspan;
            newElement.appendChild(element.getContainer());
            newElementStyle.float = 'left';
            newElementStyle.heigth = '100%';
            newElement.dataset.colspan = colspan;
            this._ui.container.appendChild(newElement);
            this._ui.elements.push(newElement);
            this.elements.push(element);

            this.refreshWidth();
        };

        Row.prototype.refreshWidth = function () {
            var elements = this._ui.elements,
                i,
                length = elements.length;
            for (i=0; i<length; i++) {
                elements[i].style.width = (0.98*(elements[i].dataset.colspan * 100 / this.count - 1 / (this.count-1))) + '%';
                if (i<length-1) {
                    elements[i].style.marginRight = '1%';
                }
            }
        };

        layout.Row = Row;
        //>>excludeStart('require', pragmas.require);
    });
    //>>excludeEnd('require');
}(document, window.Fw.layout));