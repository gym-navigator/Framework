//>>excludeStart('require', pragmas.require);
define([
    "../layout",
    "./base",
    'less!../less/layout/grid.less'
], function (layout, LayoutBase) {
    "use strict";
    //>>excludeEnd('require');
    (function (document, layout, LayoutBase) {

        var letters = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i'],
            Grid = function (options) {
            var i;
            this.options = {
                parent:options.parent,
                rows:options.rows,
                tag: options.tag || 'div'
            };
            this._ui = {
                rows:[]
            };
            this.create();
            this.setParent(options.parent);
            this.rows = [];
            if (options.rows !== undefined) {
            for (i = 0; i < options.rows.length; i++) {
                this.addRow(options.rows[i]);
            }
            }
        };

        Grid.prototype = new LayoutBase();

        Grid.prototype.create = function () {
            var ui = this._ui;
            ui.container = document.createElement(this.options.tag);
            ui.container.classList.add('ui-grid');
        };

        Grid.prototype.deleteRow = function (position) {
            var uiRows = this._ui.rows,
                rows = this.rows,
                i,
                uiRow = uiRows[position];
            uiRow.parentNode.removeChild(uiRow);
            for (i = position; i < uiRows.length; i++) {
                uiRows[i] = uiRows[i+1];
                rows[i] = rows[i+1];
            }
        };

        Grid.prototype.clear = function (from) {
            var uiRows = this._ui.rows,
                i,
                uiRow;
            from = from || 0
            for (i = from; i < uiRows.length; i++) {
                uiRow = uiRows[i];
                uiRow.parentNode.removeChild(uiRow);
            }
            this._ui.rows = uiRows.slice(0, from);
            this.rows = [];
        };

        Grid.prototype.addRow = function (elements, position, proportion) {
            var newRow = document.createElement('div'),
                newRowClassList = newRow.classList,
                element,
                elementsCount = elements.length,
                elementContainer,
                elementContainerInner,
                i,
                uiContainer = this._ui.container,
                uiRows = this._ui.rows,
                nextRow,
                rows = this.rows;
            switch (elementsCount) {
                case 1:
                    newRowClassList.add('ui-grid-line');
                    break;
                case 2:
                case 3:
                case 4:
                case 5:
                case 6:
                case 7:
                case 8:
                case 9:
                case 10:
                    newRowClassList.add('ui-grid-'+letters[elementsCount-2]);
                    break;
            }
            for (i = 0; i < elementsCount; i++) {
                element = elements[i];
                if (elementsCount === 1) {
                    elementContainerInner = newRow;
                }
                else {
                    elementContainer = document.createElement('div');
                    if (proportion && proportion[i]) {
                        elementContainer.style.width = proportion[i] + '%';
                    }
                    elementContainer.classList.add('ui-block-' + letters[i]);
                    elementContainerInner = document.createElement('div');
                    if (i<elementsCount-1) {
                        elementContainerInner.classList.add('ui-block-left');
                    }
                    elementContainer.appendChild(elementContainerInner);
                    newRow.appendChild(elementContainer);
                }
                if (element instanceof HTMLElement) {
                    elementContainerInner.appendChild(element);
                }else {
                    if (element) {
                        elementContainerInner.appendChild(element.getContainer());
                    }
                }
            }
            if (position !== undefined) {
                nextRow = uiRows[position];
                uiContainer.insertBefore(newRow, nextRow);
                for (i = uiRows.length-1; i >= position; i--) {
                    uiRows[i+1] = uiRows[i];
                    rows[i+1] = rows[i];
                }
                uiRows[position] = newRow;
                rows[position] = elements;
            }
            else {
                uiContainer.appendChild(newRow);
                uiRows.push(newRow);
                rows.push(elements);
            }
            return rows.indexOf(elements);
        };

        layout.Grid = Grid;

    }(document, window.Fw.layout, window.Fw.layout.Base));
    //>>excludeStart('require', pragmas.require);
    return layout.Grid;
});
//>>excludeEnd('require');
