//>>excludeStart('require', pragmas.require);
define([
    "../layout",
    "../layout/base",
    "../widget/button",
    "../widget/input",
    "../layout/grid",
    'less!../less/layout/popup.less'
], function (layout, Base, Button, Grid) {
    "use strict";
    //>>excludeEnd('require');

    (function (document, layout, Base, Button, Input, Grid) {
        var Popup = function (options) {
            this.options = {
                parent:options.parent || document.body,
                fixed:options.fixed || true,
                theme:options.theme || 'c',
                adv:options.adv !== undefined ? options.adv : true
            };
            this._ui = {
            };
        };

        Popup.prototype = new Base();

        Popup._ui = {};

        Popup.prototype.build = function () {
            this.create(this.options.parent || document.body);
            this.setTheme(this.options.theme);
        };

        Popup.prototype.create = function (parent) {
            var ui = this._ui;

            ui.container = document.createElement('div');
            ui.container.classList.add('ui-popup');
            ui.header = document.createElement('div');
            ui.header.classList.add('ui-header');
            ui.headerText = document.createElement('div');
            ui.headerText.classList.add('ui-header-text');
            ui.content = document.createElement('div');
            ui.content.classList.add('ui-content');
            ui.footer = document.createElement('div');
            ui.footer.classList.add('ui-footer');
            ui.header.appendChild(ui.headerText);
            ui.container.appendChild(ui.header);
            ui.container.appendChild(ui.content);
            ui.container.appendChild(ui.footer);
            parent.appendChild(ui.container);
            //contentClassList.add('sport_workout_list ui-page ui-body-c ui-page-header-fixed ui-page-footer-fixed ui-page-active');
        };

        Popup.prototype.setActive = function (value) {
            var contentClassList = this._ui.container.classList;
            if (value === true) {
                contentClassList.add('ui-popup-active');
            }
            else {
                contentClassList.remove('ui-popup-active');
            }
        };

        Popup.prototype.setTheme = function (value) {
            var contentClassList = this._ui.container.classList;
            contentClassList.add('ui-body-' + value);
        };

        Popup.prototype.setTitle = function (value) {
            this._ui.headerText.innerHTML = value;
        };

        Popup.prototype.generateForm = function(data) {
            var grid = new Grid({tag:'form'});
            this._ui = this._ui || {};
            for (i in data) {
                grid.addRow([ new Input({name:i, label:i, value: data[i]})]);
            }
            this.setContent(grid.getContainer());
        };

        Popup.prototype.addCloseButton = function (icon) {
            var self = this;
            this._ui.header_next = new Button({parent:this._ui.header, icon:icon || 'next', position:'right'});
            this._ui.header_next.onclick = function () {
                self.setActive(false);
            }
        };

        Popup.prototype.getCloseButton = function () {
            return this._ui.header_next;
        };

        Popup.prototype.setContent = function (value) {
            if (value instanceof HTMLElement) {
                this._ui.content.innerHTML = '';
                this._ui.content.appendChild(value);
            }
            else {
                this._ui.content.innerHTML = value;
            }
        };

        Popup.prototype.getContent = function () {
            return this._ui.content;
        };

        layout.Popup = Popup;
    }(document,
        window.Fw.layout,
        window.Fw.layout.Base,
        window.Fw.widget.Button,
        window.Fw.widget.Input,
        window.Fw.layout.Grid
    ));
    //>>excludeStart('require', pragmas.require);
});
//>>excludeEnd('require');
