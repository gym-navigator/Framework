//>>excludeStart('require', pragmas.require);
define([
    "../layout"
], function (layout) {
    "use strict";
    //>>excludeEnd('require');

    (function (document, layout) {
        var console = {},
            consoleUI;


        console.log = function () {
            //>>excludeStart('debug', pragmas.debug);
            var args = [].slice.call(arguments),
                i,
                html = '';
            if (!consoleUI) {
                consoleUI = document.createElement('div');
                consoleUI.style.position = 'absolute';
                consoleUI.style.bottom = '0';
                document.body.appendChild(consoleUI);
            }
            for (i = 0; i < args.length; i++) {
                if (html) {
                    html += ', ';
                }
                html += args[i];
            }
            consoleUI.innerHTML = html;
            //>>excludeEnd('debug');
        };

        layout.console = console;
    }(document, window.Fw.layout));
    //>>excludeStart('require', pragmas.require);
    return layout.console;
});
//>>excludeEnd('require');
