//>>excludeStart('require', pragmas.require);
define([
    "./core"
], function (Fw) {
    "use strict";
    //>>excludeEnd('require');
    (function (Fw) {
        Fw.model = Fw.model || {};
    }(window.Fw));
    //>>excludeStart('require', pragmas.require);
    return window.Fw.model;
});
//>>excludeEnd('require');
