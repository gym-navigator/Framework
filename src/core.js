//>>excludeStart('require', pragmas.require);
define([
], function () {
    "use strict";
    //>>excludeEnd('require');
    (function (window) {
        var Fw = {
            log : console.log.bind(console)
        };
        window.Fw = Fw;
    }(window));
    //>>excludeStart('require', pragmas.require);
    return window.Fw;
});
//>>excludeEnd('require');
