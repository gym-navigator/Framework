(function (Fw) {
    "use strict";
//>>excludeStart('require', pragmas.require);
    define([
        "./core"
    ], function (Fw) {
        //>>excludeEnd('require');
        Fw.api = Fw.api || {};
        //>>excludeStart('require', pragmas.require);
    });
//>>excludeEnd('require');
}(window.Fw));