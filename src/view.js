//>>excludeStart('require', pragmas.require);
define([
    "./core"
], function (Fw) {
    "use strict";
    //>>excludeEnd('require');
    (function (Fw) {
        Fw.view = Fw.view || {};
    }(window.Fw));
    //>>excludeStart('require', pragmas.require);
    return Fw.view;
});
//>>excludeEnd('require');
