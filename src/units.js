//>>excludeStart('require', pragmas.require);
define([
    "./core"
], function (Fw) {
    "use strict";
    //>>excludeEnd('require');
    (function (Fw) {
        var units = {
            metrics:{
                weight:'kg',
                distance:'km',
                speed:'km/h'
            },
            imperial:{
                weight:'lbs',
                distance:'mi',
                speed:'mi/h'
            },
            kg2lbs:function (number) {
                return 2.20462262 * parseFloat(number);
            },
            lbs2kg:function (number) {
                return parseFloat(number) / 2.20462262;
            },
            km2mi:function (number) {
                return 0.621371192 * parseFloat(number);
            },
            mi2km:function (number) {
                return parseFloat(number) / 0.621371192;
            },
            getDistance:function (value, type) {
                if (type === 'imperial') {
                    return units.km2mi(value).toFixed(2);
                }
                return value;
            },
            getWeight:function (value, type) {
                if (type === 'imperial') {
                    value = units.kg2lbs(value);
                } else {
                    value = parseFloat(value);
                }
                if (!value || isNaN(value)) {
                    return undefined;
                }
                return value.toFixed(0)
            },
            setDistance:function (value, type) {
                if (type === 'imperial') {
                    return units.mi2km(value).toFixed(2);
                }
                return value;
            },
            setWeight:function (value, type) {
                if (type === 'imperial') {
                    value = units.lbs2kg(value);
                } else {
                    value = parseFloat(value);
                }
                if (!value || isNaN(value)) {
                    return undefined;
                }
                return value.toFixed(0)
            }
        };
        Fw.units = units;
    }(window.Fw));
    //>>excludeStart('require', pragmas.require);
    return Fw.units;
});
//>>excludeEnd('require');
