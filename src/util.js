//>>excludeStart('require', pragmas.require);
define([
    "./core"
], function (Fw) {
    "use strict";
    //>>excludeEnd('require');
    (function (Fw) {
        Fw.util = {
            event: {
                trigger: function(element, event, data) {
                    var event = new CustomEvent(event, {
                        "detail": data
                    });
                    element.dispatchEvent(event);
                }
            }
        };
    }(window.Fw));
    //>>excludeStart('require', pragmas.require);
    return window.Fw.util;
});
//>>excludeEnd('require');
