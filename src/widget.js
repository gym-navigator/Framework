(function (Fw) {
    "use strict";
//>>excludeStart('require', pragmas.require);
    define([
        "./core"
    ], function (Fw) {
        //>>excludeEnd('require');
        Fw.widget = Fw.widget || {};
        //>>excludeStart('require', pragmas.require);
        return Fw.widget;
    });
//>>excludeEnd('require');
}(window.Fw));