(function (document, widget, Base) {
    "use strict";
//>>excludeStart('require', pragmas.require);
    define([
        "../widget",
        "./base"
    ], function (widget, Base) {
        //>>excludeEnd('require');

        var ProgressBar = function (options) {
            this.options = {
                parent:options.parent
            };
            this._ui = {};
            this.create();
            this.setParent(this.options.parent);
        };

        ProgressBar.prototype = new Base();

        ProgressBar.prototype.update = function (percent) {
            this._ui.progressInner.style.width = (percent) + '%';
        };

        ProgressBar.prototype.create = function () {
            var ui = this._ui,
                progressStyle;
            ui.container = document.createElement('div');
            progressStyle = ui.container.style;
            progressStyle.width = '100%';
            progressStyle.background = 'white';
            progressStyle.height = '30px';
            progressStyle.overflow = 'hidden';
            progressStyle.margin = '10px 0';
            ui.progressInner = document.createElement('div');
            progressStyle = ui.progressInner.style;
            progressStyle.width = '0%';
            progressStyle.background = '#c0c0c0';
            progressStyle.height = '100%';
            ui.container.appendChild(ui.progressInner);
        };

        widget.ProgressBar = ProgressBar;
        //>>excludeStart('require', pragmas.require);
        return ProgressBar;
    });
    //>>excludeEnd('require');
}(document, window.Fw.widget, window.Fw.widget.Base));