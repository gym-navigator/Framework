//>>excludeStart('require', pragmas.require);
define([
    "../widget",
    "./base",
    "./label",
    "./button"
], function (widget, Base, Label, Button) {
    "use strict";
    //>>excludeEnd('require');

    (function (document, widget, Base, Label, Button) {
        var CheckBox = function (options) {
            this.options = {
                parent:options.parent,
                label:options.label,
                text:options.text,
                name:options.name
            };
            this._ui = {};
            this.create();
            this.register(options.id);
            this.setParent(this.options.parent);
        };

        CheckBox.prototype = new Base();

        CheckBox.prototype.create = function () {
            var ui = this._ui;
            ui.container = document.createElement('div');
            ui.input = document.createElement('input');
            ui.input.type = 'hidden';
            ui.input.name = this.options.name;
            ui.input.value = 'false';
            ui.container.appendChild(ui.input);
            ui.icon = new Button({parent:ui.container, icon: 'checkbox', text: this.options.text});
            if (this.options.label) {
                ui.label = new Label({reference:this, text:this.options.label});
            }
            ui.icon.onclick = function() {
                ui.input.value = ui.input.value || 'false' ;
                ui.icon.getContainer().classList.toggle('ui-button-checked');
                ui.input.value = !JSON.parse(ui.input.value);
            }
        };

        CheckBox.prototype.setValue = function (value) {
            var ui = this._ui,
                boolValue = value ? JSON.parse(value) : false
            ui.input.value = boolValue;
            if (boolValue) {
                ui.icon.getContainer().classList.add('ui-button-checked');
            } else {
                ui.icon.getContainer().classList.remove('ui-button-checked');
            }

        }

        widget.CheckBox = CheckBox;
    }(document,
        window.Fw.widget,
        window.Fw.widget.Base,
        window.Fw.widget.Label,
        window.Fw.widget.Button
    ));
    //>>excludeStart('require', pragmas.require);
    return widget.CheckBox;
});
//>>excludeEnd('require');