//>>excludeStart('require', pragmas.require);
define([
    "../widget",
    "./base",
    "./label",
    'less!../less/widget/text.less'
], function (widget, Base, Label) {
    "use strict";
    //>>excludeEnd('require');

    (function (document, widget, Base, Label) {
        var Text = function (options) {
            this.options = {
                parent:options.parent,
                label:options.label,
                text:options.text || options.value,
                type:options.type || 'number'
            };
            this._ui = {};
            this.create();
            this.register(options.id);
            this.setParent(this.options.parent);
        };

        Text.prototype = new Base();

        Text.prototype.create = function () {
            var ui = this._ui;
            ui.container = document.createElement('div');
            ui.text = document.createElement('div');
            ui.text.classList.add('ui-text');
            ui.text.innerText = this.options.text;
            ui.container.appendChild(ui.text);
            if (this.options.label) {
                ui.label = new Label({reference:this, text:this.options.label});
            }
        };

        Text.prototype.setValue = function (value) {
            this._ui.text.innerHTML = value;
        };

        widget.Text = Text;
    }(document, window.Fw.widget, window.Fw.widget.Base, window.Fw.widget.Label));
    //>>excludeStart('require', pragmas.require);
    return widget.Text;
});
//>>excludeEnd('require');