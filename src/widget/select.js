//>>excludeStart('require', pragmas.require);
define([
    "../widget",
    "./base",
    "./label",
    'less!../less/widget/select.less'
], function (widget, Base, Label) {
    "use strict";
    //>>excludeEnd('require');

    (function (document, widget, Base, Label) {
        var widgets = {},
            widgetsCount = 0,
            Select = function (options) {
                var _options;
                this.options = {
                    parent:options.parent,
                    name:options.name,
                    label:options.label,
                    values:options.values,
                    value:options.value,
                    multi:options.multi,
                    max: options.max
                };
                _options = this.options;
                this._ui = {};
                this.create();
                this.setParent(_options.parent);
                if (_options.label) {
                    this._ui.label = new Label({reference:this, text:_options.label});
                }
                this.id = widgetsCount;
                if (widgetsCount === 0) {
                    bindEvents();
                }
                widgetsCount++;
                this.register(options.id);
                widgets[this.id] = this;
                if (_options.values) {
                    this.setValues(_options.values);
                }
                if (_options.value) {
                    this.setValue(_options.value);
                }
            };

        function onMouseDown(event) {

        }

        function onMouseUp(event) {

        }

        function onClick(event) {
            var target = event.target;
            if (target.dataset.selectValue) {
                widgets[target.dataset.widget].setValue(target.dataset.selectValue);
            }
        }

        function bindEvents() {
            document.addEventListener('mousedown', onMouseDown);
            document.addEventListener('mouseup', onMouseUp);
            document.addEventListener('click', onClick);
        }

        Select.prototype = new Base();

        Select.prototype.create = function () {
            var ui = this._ui,
                options = this.options,
                input,
                container,
                containerClassList;
            container = document.createElement('div');
            containerClassList = container.classList;
            containerClassList.add('ui-select');
            input = document.createElement('input');
            input.setAttribute('type', 'hidden');
            if (options.name) {
                input.setAttribute('name', options.name);
            }
            container.appendChild(input);
            ui.container = container;
            ui.input = input;
        };

        Select.prototype.setValues = function (values, noMax) {
            var ul,
                li,
                liInner,
                max = (!noMax && this.options.max) || 1000,
                i,
                j = 0,
                ui = this._ui,
                value = this.getValue();
            this.values = values;
            ul = document.createElement('ul');
            for (i in values) {
                if (j < max) {
                    li = document.createElement('li');
                    li.classList.add('ui-select-li');
                    liInner = document.createElement('div');
                    liInner.dataset.selectValue = i;
                    liInner.dataset.widget = this.id;
                    liInner.classList.add('ui-select-li-inner');
                    liInner.innerHTML = values[i];
                    li.appendChild(liInner);
                    ul.appendChild(li);
                    if (i === value) {
                        li.classList.add('ui-select-li-selected');
                    }
                } else if (j === max) {
                    li = document.createElement('li');
                    li.classList.add('ui-select-li');
                    liInner = document.createElement('div');
                    liInner.dataset.selectValue = 'more';
                    liInner.dataset.widget = this.id;
                    liInner.classList.add('ui-select-li-inner');
                    liInner.innerHTML = 'click to load more';
                    li.appendChild(liInner);
                    ul.appendChild(li);
                }
                j++;
            }
            if (ui.ul) {
                ui.container.removeChild(ui.ul);
            }
            ui.container.appendChild(ul);
            ui.ul = ul;
        };

        Select.prototype.getValue = function () {
            return this._ui.input.value;
        };

        Select.prototype.clear = function () {
            var ui = this._ui,
                lis = ui.ul.children,
                i,
                strvalue = '';
            for (i = lis.length - 1; i >= 0; i -= 1) {
                lis[i].classList.remove('ui-select-li-selected');
            }
            this._ui.input.value = '';
        };

        Select.prototype.setValue = function (value) {
            var ui = this._ui,
                lis = ui.ul.children,
                i,
                strvalue = '';
            if (value === 'more') {
                console.log('more');
                this.setValues(this.values, true);
            } else {
            for (i = lis.length - 1; i >= 0; i -= 1) {
                if (lis[i].firstChild.dataset.selectValue.toLowerCase() === value.toLowerCase()) {
                    if (this.options.multi) {
                        lis[i].classList.toggle('ui-select-li-selected');
                    }
                    else {
                        lis[i].classList.add('ui-select-li-selected');
                    }
                }
                else {
                    if (!this.options.multi) {
                        lis[i].classList.remove('ui-select-li-selected');
                    }
                }
                if (lis[i].classList.contains('ui-select-li-selected')) {
                    if (strvalue) {
                        strvalue += ', ';
                    }
                    strvalue += lis[i].firstChild.dataset.selectValue;
                }
            }
            if (this.options.multi) {
                ui.input.value = strvalue;
                if (this.onchange) {
                    this.onchange(strvalue);
                }
            }
            else {
                ui.input.value = value;
                if (this.onchange) {
                    this.onchange(value);
                }
            }
            }
        };

        Select.prototype.getContainer = function () {
            if (this._ui.label !== undefined) {
                return this._ui.label.getContainer();
            }
            return this._ui.container;
        };

        widget.Select = Select;
    }(document, window.Fw.widget, window.Fw.widget.Base, window.Fw.widget.Label));
    //>>excludeStart('require', pragmas.require);
    return widget.Select;
});
//>>excludeEnd('require');