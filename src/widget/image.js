//>>excludeStart('require', pragmas.require);
define([
    "../widget",
    "./base",
    "./label"
], function (widget, Base, Label) {
    "use strict";
    //>>excludeEnd('require');

    (function (document, widget, Base, Label) {
        var Image = function (options) {
            this.options = {
                parent:options.parent,
                label:options.label,
                src:options.src
            };
            this._ui = {};
            this.create();
            this.setParent(this.options.parent);
            this.bindEvents();
        };

        Image.prototype = new Base();

        Image.prototype.create = function () {
            var ui = this._ui;
            ui.container = document.createElement('div');
            ui.img = document.createElement('img');
            ui.img.classList.add('ui-image');
            if (this.options.src) {
                ui.img.src = this.options.src;
            }
            ui.container.appendChild(ui.img);
            if (this.options.label) {
                ui.label = new Label({reference:this, text:this.options.label});
            }
        };

        Image.prototype.bindEvents = function () {
            var container = this._ui.container
                self = this;
            container.addEventListener('click', function() {
                if (self.onclick) {
                    self.onclick();
                }
            });
        };

        Image.prototype.setSrc = function (src) {
            this._ui.img.src = src;
        };

        Image.prototype.setWidth = function (width) {
            this._ui.img.style.width = width;
        };

        Image.prototype.hideImage = function () {
            this._ui.img.style.display = 'none';
        };

        Image.prototype.showImage = function () {
            this._ui.img.style.display = 'block';
        };

        widget.Image = Image;
    }(document, window.Fw.widget, window.Fw.widget.Base, window.Fw.widget.Label));
    //>>excludeStart('require', pragmas.require);
    return widget.Image;
});
//>>excludeEnd('require');