//>>excludeStart('require', pragmas.require);
define([
    "../widget",
    "./base",
    "./label",
    'less!../less/widget/input.less'
], function (widget, Base, Label) {
    "use strict";
    //>>excludeEnd('require');

    (function (document, widget, Base, Label) {
        var Input = function (options) {
            var _options;
            this.options = {
                parent:options.parent,
                type:options.type || 'text',
                name:options.name,
                label:options.label,
                id:options.id,
                tag: options.tag,
                value: options.value,
                pattern: options.pattern
            };
            _options = this.options;
            this._ui = {};
            this.create();
            this.setParent(_options.parent);
            if (_options.label) {
                this._ui.label = new Label({reference:this, text:_options.label});
            }
            if (this.options.value) {
                this.setValue(this.options.value);
            }
            }, formaters = {};


        Input.prototype = new Base();

        Input.prototype.create = function () {
            var ui = this._ui,
                options = this.options,
                input,
                container,
                containerClassList;
            container = document.createElement('div');
            containerClassList = container.classList;
            containerClassList.add('ui-input-text');
            input = document.createElement(options.tag === 'textarea' ? 'textarea' : 'input');
            input.setAttribute('type', options.type);
            if (options.id) {
                input.setAttribute('id', options.id);
            }
            if (options.pattern) {
                input.setAttribute('pattern', options.pattern);
            }
            if (options.name) {
                input.setAttribute('name', options.name);
            }
            input.classList.add('ui-input-text');
            container.appendChild(input);
            ui.container = container;
            ui.input = input;
        };

        Input.prototype.getValue = function () {
            return this._ui.input.value;
        };

        Input.prototype.setValue = function (value) {
            if (this.options.tag === 'textarea') {
                this._ui.input.innerHTML = value;
            } else {
                this._ui.input.value = value;
            }
        };

        Input.prototype.getContainer = function () {
            if (this._ui.label !== undefined) {
                return this._ui.label.getContainer();
            }
            return this._ui.container;
        };

        Input.prototype.enable = function () {
            this._ui.input.disabled = false;
            this._ui.container.classList.remove('ui-input-text-disabled');
        };

        Input.prototype.disable = function () {
            this._ui.input.disabled = true;
            this._ui.container.classList.add('ui-input-text-disabled');
        };

        widget.Input = Input;
    }(document, window.Fw.widget, window.Fw.widget.Base, window.Fw.widget.Label));
    //>>excludeStart('require', pragmas.require);
    return widget.Input;
});
//>>excludeEnd('require');