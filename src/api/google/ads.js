//>>excludeStart('require', pragmas.require);
define([
    "../google",
    "../../util"
], function () {
    "use strict";
    //>>excludeEnd('require');
    (function (google, util) {
        var adsLoaded = false,
            callback,
            ads = {
                callback : function (createBanerOptions, requestAdOptions) {
                        var admobExport = window.admobExport;
                    document.removeEventListener('deviceready', callback, false);
                        if (admobExport && !adsLoaded) {
                            adsLoaded = true;
                            createBanerOptions.adSize = admobExport.AD_SIZE.SMART_BANNER;
                            admobExport.createBannerView(createBanerOptions);
                            console.log('banner request');
                            admobExport.requestAd(requestAdOptions);
                        }

                },
                init:function (createBanerOptions, requestAdOptions) {
                    //>>excludeStart('require', !pragmas.cordova);
                    var script = document.createElement('script');
                    script.src = 'https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js';
                    script.onload = function () {
                        util.event.trigger(document, 'adsloaded');
                    };
                    document.body.appendChild(script);
                    //>>excludeEnd('require');
                    //>>excludeStart('require', pragmas.cordova);
                    callback = ads.callback.bind(null, createBanerOptions, requestAdOptions);
                    document.addEventListener('deviceready', callback, false);
                    //>>excludeEnd('require');
                },
                show:function () {
                    //>>excludeStart('require', !pragmas.cordova);
                    if (window.adsbygoogle) {
                        window.adsbygoogle.push({});
                    }
                    else {
                        document.addEventListener('adsloaded', function () {
                            window.adsbygoogle.push({});
                        });
                    }
                    //>>excludeEnd('require');
                }
            };

        google.ads = ads;
    }(window.Fw.api.google,
        window.Fw.util));
    //>>excludeStart('require', pragmas.require);
});
//>>excludeEnd('require');
