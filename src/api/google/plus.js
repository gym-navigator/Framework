//>>excludeStart('require', pragmas.require);
define([
    "../google",
    "../../remote"
], function () {
    "use strict";
    //>>excludeEnd('require');
    (function (google, remote) {
        var plus = {
            init:function () {
                if (!window.cordova) {
                    var script = document.createElement('script');
                    script.src = 'https://apis.google.com/js/client:plusone.js';
                    document.body.appendChild(script);
                }
            },
            login:function (options, callback) {

                    //>>excludeStart('require', !pragmas.cordova);
                    var params = {
                        cookiepolicy: 'single_host_origin',
                        scope: options.scope,
                        clientid: options.webClientId,
                        'callback': callback
                    };
                    gapi.auth.signIn(params);
                //>>excludeEnd('require');
                //>>excludeStart('require', pragmas.cordova);
                    var authUrl = 'https://accounts.google.com/o/oauth2/auth?' +
                        'client_id=' + encodeURIComponent(options.clientId) +
                        '&redirect_uri=' + encodeURIComponent('http://localhost') +
                        '&response_type=code' +
                        '&scope=' + encodeURIComponent(options.scope);

                    //Open the OAuth consent page in the InAppBrowser
                    var authWindow = openWindow(authUrl, 'gp', '');

                    //The recommendation is to use the redirect_uri "urn:ietf:wg:oauth:2.0:oob"
                    //which sets the authorization code in the browser's title. However, we can't
                    //access the title of the InAppBrowser.
                    //
                    //Instead, we pass a bogus redirect_uri of "http://localhost", which means the
                    //authorization code will get set in the url. We can access the url in the
                    //loadstart and loadstop events. So if we bind the loadstart event, we can
                    //find the authorization code and close the InAppBrowser after the user
                    //has granted us access to their data.
                    authWindow.addEventListener('loadstart', function (e) {
                        console.log(e);
                        var url = e.url;
                        var code = /\?code=(.+)$/.exec(url);
                        var error = /\?error=(.+)$/.exec(url);

                        if (code || error) {
                            //Always close the browser when match is found
                            authWindow.close();
                        }

                        if (code) {
                            //Exchange the authorization code for an access token
                            remote.getJSON('https://accounts.google.com/o/oauth2/token', {
                                code:code[1],
                                client_id:options.clientId,
                                client_secret:options.clientSecret,
                                redirect_uri:'http://localhost',
                                grant_type:'authorization_code'
                            }, callback, 'POST');
                        } else if (error) {
                            //The user denied access to the app
                        }
                    });
                //>>excludeEnd('require');
            }
        };

        google.plus = plus;
    }(window.Fw.api.google,
        window.Fw.remote
    ));
    //>>excludeStart('require', pragmas.require);
});
//>>excludeEnd('require');
