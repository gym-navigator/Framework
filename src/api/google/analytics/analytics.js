(function (google) {
    "use strict";
//>>excludeStart('require', pragmas.require);
    define([
        "../../google"
    ], function () {
        //>>excludeEnd('require');
        var Analytics = function (options) {
            this.options = {
                clientId: options.clientId,
                apiKey: options.apiKey,
                scopes: options.scopes,
                accountId: options.accountId,
                webPropertyId: options.webPropertyId,
                profileId: options.profileId
            };
            this.handleAuthorized = options.handleAuthorized;
            this.data = {};
        };

        Analytics.prototype.authorize = function() {
            gapi.client.setApiKey(this.options.apiKey);
            gapi.auth.authorize({client_id:this.options.clientId, scope:this.options.scopes, immediate:true}, this.handleAuthResult.bind(this));
        };

        Analytics.prototype.handleAuthResult = function(authResult) {
            var self = this;
            this.authResult = authResult;
            if (authResult) {
                gapi.client.load('analytics', 'v3', function () {
                    if (self.handleAuthorized !== undefined) {
                        self.handleAuthorized();
                    }
                });
            }
        };

        Analytics.prototype.queryAccounts = function() {
            gapi.client.analytics.management.accounts.list().execute(this.handleAccounts.bind(this));
        };

        Analytics.prototype.handleAccounts = function(results) {
            if (!results.code) {
                if (results.items && results.items.length) {
                    this.data.accounts = results.items;
                }
            }
        };

        Analytics.prototype.queryWebProperties = function(accountId) {
            accountId = accountId || this.options.accountId || this.data.accounts[0].id;
            gapi.client.analytics.management.webproperties.list({
                'accountId':accountId
            }).execute(this.handleWebProperties.bind(this));
        };

        Analytics.prototype.handleWebProperties = function(results) {
            if (!results.code) {
                if (results.items && results.items.length) {
                    this.data.webProperties = results.items;
                }
            }
        };

        Analytics.prototype.queryProfiles = function(accountId, webPropertyId) {
            accountId = accountId || this.options.accountId || this.data.accounts[0].id;
            webPropertyId = webPropertyId || this.options.webpropertyId || this.data.webProperties[0].id;
            gapi.client.analytics.management.profiles.list({
                'accountId':accountId,
                'webPropertyId':webPropertyId
            }).execute(this.handleProfiles.bind(this));
        };

        Analytics.prototype.handleProfiles = function(results) {
            if (!results.code) {
                if (results.items && results.items.length) {
                    this.data.profiles = results.items;
                }
            }
        };

        Analytics.prototype.query = function(options, handle) {
            if (!options.ids) {
                options.ids = 'ga:' + this.options.profileId;
            }
            gapi.client.analytics.data.ga.get(options).execute(handle);
        };

        google.Analytics = Analytics;
        //>>excludeStart('require', pragmas.require);
    });
    //>>excludeEnd('require');
}(window.Fw.api.google));