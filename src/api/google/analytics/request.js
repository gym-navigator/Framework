(function (analytics, util) {
    "use strict";
//>>excludeStart('require', pragmas.require);
    define([
        "../analytics",
        "../../../util"
    ], function (analytics, util) {
        //>>excludeEnd('require');
        var gaPlugin,
            request = {
                successHandler:function () {
                },
                errorHandler:function () {
                },
                init:function (options) {
                    //>>excludeStart('require', !pragmas.cordova);
                    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
                        (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
                    m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
                    })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
                    ga('create', 'UA-41941504-3', 'gymnavigator.com');
                    util.event.trigger(document, 'gaready');
                    //>>excludeEnd('require');
                    //>>excludeStart('require', pragmas.cordova);
                    gaPlugin = window.plugins.gaPlugin;
                    options.successHandler = options.successHandler || request.successHandler;
                    options.errorHandler = options.errorHandler || request.errorHandler;
                    gaPlugin.init(options.successHandler, options.errorHandler, options.clientId, 10);
                    //>>excludeEnd('require');
                },
                event:function (options) {
                    var resultHandler = options.resultHandler || function () {
                        },
                        errorHandler = options.errorHandler || function () {
                        },
                        category = options.category || '',
                        action = options.action || '',
                        label = options.label || '',
                        value = options.value || 0;
                    //>>excludeStart('require', !pragmas.cordova);
                    if (window.ga !== undefined) {
                        ga('send', 'event', category, action, label, value);
                    }
                    else {
                        document.addEventListener('gaready', function () {
                            ga('send', 'event', category, action, label, value);
                        });
                    }
                    //>>excludeEnd('require');
                    //>>excludeStart('require', pragmas.cordova);
                    if (gaPlugin) {
                        gaPlugin.trackEvent(resultHandler, errorHandler, category, action, label, value);
                    } else {
                        document.addEventListener('deviceready', function () {
                            gaPlugin.trackEvent(resultHandler, errorHandler, category, action, label, value);
                        });
                    }
                    //>>excludeEnd('require');
                },
                variable:function (options) {
                    var resultHandler = options.resultHandler || function () {
                        },
                        errorHandler = options.errorHandler || function () {
                        },
                        index = options.index || 0,
                        value = options.value || null;
                    //>>excludeStart('require', !pragmas.cordova);
                    //>>excludeEnd('require');
                    //>>excludeStart('require', pragmas.cordova);
                    if (gaPlugin) {
                        gaPlugin.setVariable(resultHandler, errorHandler, index, value);
                    } else {
                        document.addEventListener('deviceready', function () {
                            gaPlugin.setVariable(resultHandler, errorHandler, index, value);
                        });
                    }
                    //>>excludeEnd('require');
                },
                page:function (options) {
                    var resultHandler = options.resultHandler || function () {
                        },
                        errorHandler = options.errorHandler || function () {
                        },
                        url = options.url || '';
                    //>>excludeStart('require', !pragmas.cordova);
                    if (window.ga !== undefined) {
                        ga('send', 'pageview', url);
                    } else {
                        document.addEventListener('gaready', function () {
                            ga('send', 'pageview', url);
                        });
                    }
                    //>>excludeEnd('require');
                    //>>excludeStart('require', pragmas.cordova);
                    if (gaPlugin) {
                        gaPlugin.trackPage(resultHandler, errorHandler, url);
                    } else {
                        document.addEventListener('deviceready', function () {
                            gaPlugin.trackPage(resultHandler, errorHandler, url);
                        });
                    }
                    //>>excludeEnd('require');
                },
                exit:function (options) {
                    var resultHandler = options.resultHandler || function () {
                        },
                        errorHandler = options.errorHandler || function () {
                        };
                    //>>excludeStart('require', !pragmas.cordova);

                    //>>excludeEnd('require');
                    //>>excludeStart('require', pragmas.cordova);
                    if (gaPlugin) {
                        gaPlugin.exit(resultHandler, errorHandler);
                    } else {
                        document.addEventListener('deviceready', function () {
                            gaPlugin.exit(resultHandler, errorHandler);
                        });
                    }
                    //>>excludeEnd('require');
                }
            };

        analytics.request = request;
        //>>excludeStart('require', pragmas.require);
    });
    //>>excludeEnd('require');
}(window.Fw.api.google.analytics,
    window.Fw.util
));