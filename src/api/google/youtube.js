(function (google) {
    "use strict";
//>>excludeStart('require', pragmas.require);
    define([
        "../google"
    ], function () {
        //>>excludeEnd('require');
        var YouTube = function (options) {
                this.options = {};
                this.data = {};
            },
            youTubeLoaded = false,
            queue = [];


        YouTube.prototype._query = function (query, callback, options) {
            options = options || {};
            options.q = query;
            var request = gapi.client.youtube.search.list(options);
            request.execute(callback);
        };

        YouTube.prototype.query = function (query, callback, options) {
            if (youTubeLoaded) {
                this._query(query, callback, options);
            } else {
                queue.push(this._query.bind(this, query, callback, options));
            }
        };

        window.googleApiClientReady = function () {
            gapi.client.load('youtube', 'v3', function () {
                var call;
                youTubeLoaded = true;
                while(queue.length) {
                    call = queue.pop();
                    call();
                }
            });
        }

        google.YouTube = YouTube;
        //>>excludeStart('require', pragmas.require);
    });
    //>>excludeEnd('require');
}(window.Fw.api.google));
