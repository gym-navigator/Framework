(function (api) {
    "use strict";
//>>excludeStart('require', pragmas.require);
    define([
        "../api"
    ], function () {
        //>>excludeEnd('require');
        api.google = api.google || {};

        //>>excludeStart('require', pragmas.require);
    });
//>>excludeEnd('require');
}(window.Fw.api));