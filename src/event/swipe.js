//>>excludeStart('require', pragmas.require);
define([
    "../event"
], function (event) {
    "use strict";
    //>>excludeEnd('require');
    (function (document, event) {
        var swipe = {
            mousedown: 'mousedown',
            mouseup: 'mouseup',
            distance: 100,
            startPoint: {
                x:0,
                y:0
            },
            mousedownHandler: function(event) {
                this.startPoint = {
                    x: event.clientX,
                    y: event.clientY
                };
            },
            mouseupHandler: function(event) {
            },
            trigger: function(element, event, data) {

            }
        };
        document.body.addEventListener(swipe.mousedown, swipe.mousedownHandler.bind(swipe));
        document.body.addEventListener(swipe.mouseup, swipe.mouseupHandler.bind(swipe));
        event.swipe = swipe;
    }(document, window.Fw.event));
    //>>excludeStart('require', pragmas.require);
    return event.swipe;
});
//>>excludeEnd('require');
