//>>excludeStart('require', pragmas.require);
define([
    "../user",
    "fw/layout/page",
    "fw/widget/button",
    "fw/widget/input"
], function (user, Page, Button, Input) {
    "use strict";
    //>>excludeEnd('require');
    (function (document, user, Page, Button, Input) {

        var Login = function () {
            var ui,
                form,
                content;
            this.build();
            this._ui = this._ui || {};
            ui = this._ui;
            this.setTitle('Login');
            ui.header_home = new Button({
                parent:this.getHeader(),
                icon:'home',
                position:'left'});
            ui.content_info = document.createElement('div');
            form = document.createElement('form');
            ui.content_form = form;
            ui.content_email = new Input({
                parent:form,
                label:'EMAIL',
                type: 'email',
                name:'_username'});
            ui.content_password = new Input({
                parent:form,
                label:'PASSWORD',
                type:'password',
                name:'_password'});
            ui.content_submit = new Button({
                parent:form,
                text:'LOGIN'
            });
            content = ui.content;
            content.appendChild(ui.content_info);
            content.appendChild(form);
        };
        Login.prototype = new Page({
            parent:document.body,
            fixed:true,
            theme:'c'

        });
        Login.prototype.getHomeButton = function () {
            return this._ui.header_home;
        };
        Login.prototype.getFormButton = function () {
            return this._ui.content_submit.getContainer();
        };
        Login.prototype.getForm = function () {
            return this._ui.content_form;
        };
        Login.prototype.setInfo = function (text) {
            return this._ui.content_info.innerHTML = text;
        };
        user.Login = Login;
    }(document, window.Fw.view.user, window.Fw.layout.Page, window.Fw.widget.Button, window.Fw.widget.Input));
    //>>excludeStart('require', pragmas.require);
    return user.Sync;
});
//>>excludeEnd('require');
