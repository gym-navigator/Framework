//>>excludeStart('require', pragmas.require);
define([
    "../view"
], function (view) {
    "use strict";
    //>>excludeEnd('require');
    (function (view) {
        view.user = view.user || {};
    }(window.Fw.view));
    //>>excludeStart('require', pragmas.require);
    return view.user;
});
//>>excludeEnd('require');
