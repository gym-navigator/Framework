//>>excludeStart('require', pragmas.require);
define([
    "../error",
    "./base",
    "../remote"
], function (error, Base, remote) {
    "use strict";
    //>>excludeEnd('require');

    (function (document, error, Base, remote, GNversion) {
        var UserVoiceError = function () {
        };

        UserVoiceError.prototype = new Base();

        UserVoiceError.prototype.error = function (errorMsg, url, lineNumber, columnNumber, errorObj) {
            var uvSubdomain = "gymnavigator";
            var uvKey = "flGdYZwyKaftIgteGHx1mw";
            var subject = '[' + GNversion + '] ' + errorMsg+' ('+url+":"+lineNumber+':'+columnNumber+')';
            var name = 'Automate error request';
            var email = 'auto@gymnavigator.com';

// Execute the JSONP request to submit the ticket
            remote.getJSON('https://' + uvSubdomain + '.uservoice.com/api/v1/tickets/create_via_jsonp.json?',
                {
                    client:uvKey,
                    ticket:{
                        message:errorObj && errorObj.stack,
                        subject:subject,
                        referrer: location && location.href,
                        user_agent: window && window.navigator && window.navigator.userAgent
                    },
                    format: 'json',
                    name:name,
                    email:email
                }, function () {
                    console.log('Successfully submitted ticket!'); // You might want to redirect the user here, or show a stylized message to them.
                }, 'GET');
            return false;
        };

        error.UserVoiceError = UserVoiceError;
    }(document, window.Fw.error,
        window.Fw.error.Base,
        window.Fw.remote,
        '1.0.0'
    ));
    //>>excludeStart('require', pragmas.require);
    return error.UserVoiceError;
});
//>>excludeEnd('require');
