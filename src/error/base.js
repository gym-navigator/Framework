//>>excludeStart('require', pragmas.require);
define([
    "../error"
], function (error) {
    "use strict";
    //>>excludeEnd('require');

    (function (document, error) {
        var Base = function () {
            },
            errorClass = null;

        Base.prototype.register = function () {
            errorClass = this;
        };

        window.onerror = function myErrorHandler(errorMsg, url, lineNumber, columnNumber, errorObj) {
            if (errorClass) {
                return errorClass.error(errorMsg, url, lineNumber, columnNumber, errorObj);
            }
            return false;
        };

        error.Base = Base;
    }(document, window.Fw.error));
    //>>excludeStart('require', pragmas.require);
    return error.Base;
});
//>>excludeEnd('require');
