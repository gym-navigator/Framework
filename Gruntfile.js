module.exports = function (grunt) {
    'use strict';
    var lessFiles = [];
    grunt.initConfig({
        pkg:grunt.file.readJSON('package.json'),
        sprite:{
            all:{
                src:'web/images/sprites/*.png',
                destImg:'web/images/sprite.png',
                destCSS:'web/less/sprites.less'
            }
        },
        blanket_qunit:{
            all:{
                options:{
                    urls:['test/test.html?coverage=true&gruntReport'],
                    threshold:70
                }
            }
        },
        clean:{
            build:{
                src:["build", 'tmp']
            },
            android:{
                src:['cordova/platforms/android/assets/www']
            }
        },
        plato:{
            app:{
                files:{
                    report:['src/**/*.js']
                }
            }
        },
        complexity:{
            generic:{
                src:['src/**/*.js', '../js/dev/fw/**/*.js'],
                options:{
                    jsLintXML:'report/report.xml', // create XML JSLint-like report
                    checkstyleXML:'report/checkstyle.xml', // create checkstyle report
                    errorsOnly:false, // show only maintainability errors
                    cyclomatic:3,
                    halstead:8,
                    maintainability:100
                }
            }
        },
        imagemin:{                          // Task
            dynamic:{                         // Another target
                files:[
                    {
                        expand:true, // Enable dynamic expansion
                        cwd:'web/images/', // Src matches are relative to this path
                        src:['**/*.{png,jpg,gif}'], // Actual patterns to match
                        dest:'web/img/'                  // Destination path prefix
                    }
                ]
            }
        },
        'string-replace':{
            tmpl:{
                files:{
                    'tmp/js/tpl.js':'tmp/js/tpl.js' // includes files in dir
                },
                options:{
                    replacements:[
                        {
                            pattern:"define(function(){",
                            replacement:"//>>excludeStart('require', pragmas.require);\n" +
                                "define(function(){\n" +
                                "//>>excludeEnd('require');\n"
                        },
                        {
                            pattern:/[\s]*return this\["App"\]\["templates"\]\;[\s]*\}\);[\s]*/gi,
                            replacement:"\n//>>excludeStart('require', pragmas.require);\n" +
                                "return window.App.templates;\n" +
                                "});\n" +
                                "                            //>>excludeEnd('require');"
                        }
                    ]
                }
            },
            dist:{
                files:{
                    'build/www/<%= pkg.build %>/css/prod.min.css':'build/www/<%= pkg.build %>/css/prod.css',
                    'build/fb/<%= pkg.build %>/css/prod.min.css':'build/fb/<%= pkg.build %>/css/prod.css',
                    'build/android/<%= pkg.build %>/css/prod.min.css':'build/android/<%= pkg.build %>/css/prod.css',
                },
                options:{
                    replacements:[
                        {
                            pattern:/images/ig,
                            replacement:'img'
                        }
                    ]
                }
            },
            build:{
                files:{
                    'build/www/index.html':'src/index-build.html',
                    'build/fb/index.php':'src/index-build.html',
                    'test/test.html':'src/test.html'
                },
                options:{
                    replacements:[
                        {
                            pattern:"__build__",
                            replacement:"<%= pkg.build %>"
                        }
                    ]
                }
            },
            android:{
                files:{
                    'build/android/index.html':'src/index-android.html' // includes files in dir
                },
                options:{
                    replacements:[
                        {
                            pattern:"__build__",
                            replacement:"<%= pkg.build %>"
                        }
                    ]
                }
            }
        },
        qunit_junit:{
            options:{
                dest:'reports/test-reports'
            }
        },
        uglify:{
            options:{
                mangle:false
            },
            my_target:{
                files:{
                    'build/fw.min.js':['build/fw.js']
                }
            }
        },

        requirejs:{
            fw:{
                options:{
                    baseUrl:"src/",
                    name:"fw",
                    out:"build/fw.js",
                    pragmasOnSave:{
                        //Just an example
                        require:true,
                        debug:true,
                        www:false,
                        fb:true,
                        cordova:true
                    },
                    optimize:'none',
                    useStrict:true,
                    paths:{
                        'lib':'../lib',
                        'less-root':'../less'
                    },
                    map:{
                        '*':{
                            'less':'../lib/empty-less/less' // path to less
                        }
                    },
                    done:function (done, output) {
                        var files = output.split("\n").filter(function (name) {
                            return name.indexOf('less!') > -1;
                        }).map(function (name) {
                                return (name.replace('../lib/empty-less/less!', '')
                                    .replace(/^fw/, 'src')
                                    .replace(/^less-root/, '../less'));
                            });

                        files.forEach(function (value) {
                            var key = 'tmp/css/' + value.replace(/\//g, '_').replace(/^\.+/g, '') + '.css',
                                obj = {};
                            obj[key] = 'src/js/' + value + '.less';
                            lessFiles.push(obj);
                        });
                        grunt.config.set('less.development.files', lessFiles);
                        grunt.log.ok(lessFiles);
                        done();
                    }
                }
            },
        },
        less:{
            development:{
                options:{
                    path:["build/css"]
                },
                files:lessFiles
            }
        },
        cssmin:{
            minify:{
                files:{
                    'build/fw.min.css':['tmp/css/*.css'],
                }
            }
        },
        'closure-compiler':{
            fw:{
                closurePath:'./',
                js:'build/fw.js',
                jsOutputFile:'build/fw.min.js',
                maxBuffer:500,
                options:{
                    compilation_level:'ADVANCED_OPTIMIZATIONS',
                    language_in:'ECMASCRIPT5_STRICT',
                    debug:false,
                    formatting:'PRETTY_PRINT'
                }
            }
        }
    });

    grunt.config('pkg.build', parseInt(grunt.config('pkg.build')) + 1);
    grunt.file.write('package.json', JSON.stringify(grunt.config('pkg')));
    grunt.loadNpmTasks('assemble-less');
    grunt.loadNpmTasks('grunt-string-replace');
    grunt.loadNpmTasks('grunt-contrib-requirejs');
    grunt.loadNpmTasks('grunt-closure-compiler');
    grunt.loadNpmTasks('grunt-contrib-cssmin');
    grunt.loadNpmTasks('grunt-plato');
    grunt.loadNpmTasks('grunt-blanket-qunit');
    grunt.loadNpmTasks('grunt-qunit-junit');
    grunt.loadNpmTasks('grunt-complexity');
    grunt.loadNpmTasks('grunt-contrib-clean');
    grunt.registerTask('test', ['qunit_junit', 'blanket_qunit']);
    grunt.registerTask('default', ['requirejs']);
};
